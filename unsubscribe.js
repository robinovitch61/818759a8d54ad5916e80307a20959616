// https://caiorss.github.io/bookmarklet-maker/
const anchors = Array.from(document.getElementsByTagName("a"))
console.log(anchors);
const unsubscribe = anchors.find(anchor => anchor.innerText.toLowerCase().includes("unsubscribe")).cloneNode(true)
unsubscribe.innerText = "UNSUBSCRIBE"
unsubscribe.style.fontSize = "50px"
document.body.prepend(unsubscribe)
